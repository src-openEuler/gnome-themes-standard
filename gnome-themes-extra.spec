Name:           gnome-themes-extra
Version:        3.28
Release:        1
Summary:        GNOME Extra Themes

License:        LGPLv2+
URL:            https://gitlab.gnome.org/GNOME/gnome-themes-extra
Source0:        https://download.gnome.org/sources/%{name}/3.28/%{name}-%{version}.tar.xz
Source1:        gtkrc

BuildRequires:  gcc
BuildRequires:  gettext
BuildRequires:  intltool
BuildRequires:  make
BuildRequires:  pkgconfig(gtk+-2.0)
BuildRequires:  pkgconfig(gtk+-3.0)
BuildRequires:  pkgconfig(librsvg-2.0)
BuildRequires:  /usr/bin/gtk-update-icon-cache

Recommends: (adwaita-gtk2-theme = %{version}-%{release} if gtk2)
Requires: adwaita-icon-theme
Requires: highcontrast-icon-theme = %{version}-%{release}

Obsoletes: gnome-themes-standard < 3.28
Provides: gnome-themes-standard = %{version}-%{release}
Provides: gnome-themes-standard%{_isa} = %{version}-%{release}

%description
This module houses themes and theme-y tidbits that don’t really fit in anywhere
else, or deserve their own module. At the moment this consists of:

 * The GTK+ 2 version of Adwaita
 * Adwaita-dark as a separate theme, along with its GTK+ 2 equivalent
 * GTK+ 2 versions of the HighContrast themes
 * The legacy HighContrast icon theme
 * Index files needed for Adwaita to be used outside of GNOME

Once named gnome-themes-standard, this module used to contain various
components of the default GNOME 3 theme. However, at this point, most it has
moved elsewhere. The GTK+ 3 versions of the Adwaita and HighContrast themes are
now part of GTK+ 3 itself, and the HighContrastInverse and LowConstrast themes
have been discontinued.

Not to be confused with gnome-themes-extras.

%package -n adwaita-gtk2-theme
Summary: Adwaita gtk2 theme
Requires: gtk2%{_isa}
# cursor and icon themes required for the theme
Requires: adwaita-cursor-theme
Requires: adwaita-icon-theme
Requires: highcontrast-icon-theme = %{version}-%{release}

%description -n adwaita-gtk2-theme
The adwaita-gtk2-theme package contains a gtk2 theme for presenting widgets
with a GNOME look and feel.

%package -n highcontrast-icon-theme
Summary: HighContrast icon theme
BuildArch: noarch
# Split out to a new subpackage in gnome-themes-standard 3.28-12
Conflicts: gnome-themes-standard < 3.28

%description -n highcontrast-icon-theme
This package contains the HighContrast icon theme used by the GNOME desktop.

%prep
%autosetup -p1

%build
%configure
%make_build

%install
%make_install
find $RPM_BUILD_ROOT -name '*.la' -delete

rm -f $RPM_BUILD_ROOT%{_datadir}/icons/HighContrast/icon-theme.cache
touch $RPM_BUILD_ROOT%{_datadir}/icons/HighContrast/icon-theme.cache

mkdir -p $RPM_BUILD_ROOT%{_datadir}/gtk-2.0
cp -a $RPM_SOURCE_DIR/gtkrc $RPM_BUILD_ROOT%{_datadir}/gtk-2.0/gtkrc

%transfiletriggerin -n highcontrast-icon-theme -- %{_datadir}/icons/HighContrast
gtk-update-icon-cache --force %{_datadir}/icons/HighContrast &>/dev/null || :

%transfiletriggerpostun -n highcontrast-icon-theme -- %{_datadir}/icons/HighContrast
gtk-update-icon-cache --force %{_datadir}/icons/HighContrast &>/dev/null || :

%files
%license LICENSE
%doc NEWS README.md
%{_datadir}/themes/Adwaita/gtk-3.0/
%{_datadir}/themes/Adwaita-dark/gtk-3.0/
%{_datadir}/themes/HighContrast/gtk-3.0/

%files -n highcontrast-icon-theme
%license LICENSE
%dir %{_datadir}/icons/HighContrast
%{_datadir}/icons/HighContrast/16x16/
%{_datadir}/icons/HighContrast/22x22/
%{_datadir}/icons/HighContrast/24x24/
%{_datadir}/icons/HighContrast/32x32/
%{_datadir}/icons/HighContrast/48x48/
%{_datadir}/icons/HighContrast/256x256/
%{_datadir}/icons/HighContrast/scalable/
%{_datadir}/icons/HighContrast/index.theme
%ghost %{_datadir}/icons/HighContrast/icon-theme.cache

%files -n adwaita-gtk2-theme
%license LICENSE
%{_libdir}/gtk-2.0/2.10.0/engines/libadwaita.so
%{_datadir}/gtk-2.0/gtkrc
%dir %{_datadir}/themes/Adwaita
%{_datadir}/themes/Adwaita/gtk-2.0/
%{_datadir}/themes/Adwaita/index.theme
%dir %{_datadir}/themes/Adwaita-dark
%{_datadir}/themes/Adwaita-dark/gtk-2.0/
%{_datadir}/themes/Adwaita-dark/index.theme
%dir %{_datadir}/themes/HighContrast
%{_datadir}/themes/HighContrast/gtk-2.0/
%{_datadir}/themes/HighContrast/index.theme

%changelog
* Tue Jan 10 2023 Wenlong Ding <wenlong.ding@turbolinux.com.cn> - 3.28-1
- upgrade to 3.28

* Thu May 19 2022 loong_C <loong_c@yeah.net> - 3.27.90-5
- fix spec changelog date

* Tue Jul 20 2021 liuyumeng <liuyumeng5@huawei.com> - 3.27.90-4
- delete gdb in buildrequires

* Fri Mar 20 2020 songnannan <songnannan2@huawei.com> - 3.27.90-3
- add gdb in buildrequires

* Tue Sep 17 2019 chenzhenyu <chenzhenyu13@huawei.com> - 3.27.90-2
- Package init
